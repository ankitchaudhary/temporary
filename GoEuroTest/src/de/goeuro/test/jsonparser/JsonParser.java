package de.goeuro.test.jsonparser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;
import javax.json.JsonValue;

//=== import Packages ===========================================

/**
 * JsonParser is the class used for querying the Json API for extracting
 * location information about a city and store the information into the CSV
 * file.
 * 
 * User has to provide the location name as input to the program using java run
 * time argument
 * <p>
 * <p>
 * Creation date: Aug 14, 2014
 * 
 * @version 1.0 Aug 14, 2014
 * @author Ankit Chaudhary
 */
public class JsonParser {

	// === Constants =============================================

	/**
	 * <code>keysJsonObject</code>: Keys from json object contained in the array
	 * returned
	 */
	private static final String[] keysJsonObject = { "_type", "_id", "name",
			"type" };
	/** <code>keysGeoLocation</code>: Keys from geo_location object */
	private static final String[] keysGeoLocation = { "latitude", "longitude" };
	/** <code>apiLocation</code>: API Location */
	private static final String apiLocation = "http://api.goeuro.com/api/v2/position/suggest/en/";
	/**
	 * <code>pattern</code>: For checking that name of the location does not
	 * contain and special or numerical character
	 */
	private static final Pattern pattern = Pattern.compile("[^a-z ]",
			Pattern.CASE_INSENSITIVE);

	// === Fields ================================================

	/** <code>printWriter</code>: For writing to the file */
	private static PrintWriter printWriter = null;
	private static String fileName = null;

	// === Methods ===============================================

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length == 0 || "".equals(args[0])
				|| pattern.matcher(args[0]).find()) {
			System.err
					.println("Bad location name. Please do not give empty location or location containing special or numerical characters. ");
			System.err
					.println("Following are good examples of location names: 'Berlin', 'Kaiserslautern', \"New York\" !!!");
			System.out
					.println("Try to execute following: 'java -jar GoEuroTest.jar \"<LocationName>\"' ");
		} else if (args.length > 1) {
			System.err
					.println("Looks like you have supplied too many arguments !!!");
			System.out
					.println("Try to execute following: 'java -jar GoEuroTest.jar \"<LocationName>\"' ");
		} else {
			args[0] = args[0].replaceAll("\\s", "");
			System.out.println("Searching for location " + args[0]);
			if (!findLocation(args[0])) {
				System.err.println("Unable to parse API response.");
				System.exit(-1);
			}
			System.out.println("Location details save in file with name "
					+ args[0] + ".csv");
		}

	}

	/**
	 * For querying the location API and reading the buffer response containing
	 * JSON document. It will create JsonArray from the document and traverse
	 * the array to extract the required information.
	 * 
	 * @param locationName
	 * @return boolean
	 */
	private static boolean findLocation(String locationName) {

		InputStream inputStream = queryJsonAPI(locationName);

		if (null == inputStream)
			return false;

		// Reading the response from API

		JsonReader reader = Json.createReader(new BufferedReader(
				new InputStreamReader(inputStream)));
		JsonStructure jsonst = reader.read();

		if ("ARRAY".equals(jsonst.getValueType().toString())) {

			fileName = locationName;
			JsonArray jsonArray = (JsonArray) jsonst;
			for (JsonValue jsonValue : jsonArray) {
				writeToCSVFile(extractContent((JsonObject) jsonValue));
			}
			printWriter.close();
			return true;
		} else {
			System.err
					.println("Expecting Json Array as the response but received "
							+ jsonst.getValueType()
							+ ". Unable to do further processing terminating the execution !!!");
			return false;
		}
	}

	/**
	 * Query JSON location API using location name
	 * 
	 * @param locationName
	 * @return InputStream
	 */
	private static InputStream queryJsonAPI(String locationName) {

		try {
			URL url = new URL(apiLocation + locationName);
			return url.openStream();
		} catch (Exception e) {

			if (e instanceof MalformedURLException)
				System.err
						.println("Oopsss !!! Malformed URL Exception occured !!!");
			if (e instanceof IOException)
				System.err.println("Oopsss !!! IOException occured ");
			return null;
		}

	}

	/**
	 * For extracting the required values out of the JSON document
	 * 
	 * @param jsonObject
	 * @return String containing content to be stored inside the .csv file
	 */
	private static String extractContent(JsonObject jsonObject) {
		String fileContent = "";
		JsonObject geo_position = jsonObject.getJsonObject("geo_position");

		for (String key : keysJsonObject) {
			if (jsonObject.get(key) != null)
				fileContent = fileContent + jsonObject.get(key).toString()
						+ ",";
			else
				fileContent = fileContent + ",";
		}

		for (String key : keysGeoLocation) {
			if (geo_position.get(key) != null)
				fileContent = fileContent + geo_position.get(key).toString()
						+ ",";
			else
				fileContent = fileContent + ",";
		}

		// Trimming the file content

		fileContent = fileContent.substring(0, fileContent.lastIndexOf(","));
		return fileContent;
	}

	/**
	 * For writing the content to the CSV file.
	 * 
	 * @param fileContent
	 * @return boolean
	 */
	private static boolean writeToCSVFile(String fileContent) {

		// Writing the content to the csv file

		try {
			if (null == printWriter) {
				File file = new File(fileName + ".csv");
				if (file.exists()) {
					file.delete();
				}
				printWriter = new PrintWriter(new BufferedWriter(
						new FileWriter(fileName + ".csv", true)));
				printWriter.println("_type,_id,name,type,latitude,longitude");
			}
			printWriter.println(fileContent);
			return true;
		} catch (IOException e) {
			System.err.println("IOException occured while writing the content "
					+ e.getMessage());
			e.printStackTrace();
			return false;
		}

	}
}
